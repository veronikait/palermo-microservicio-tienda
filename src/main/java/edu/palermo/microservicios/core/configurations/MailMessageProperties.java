package edu.palermo.microservicios.core.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "mail")
@PropertySource(value = "classpath:mail.properties")
@Data
public class MailMessageProperties {

    private String desde;
    private String destinatarios;
    private String asunto;
    private String mensaje;

}
