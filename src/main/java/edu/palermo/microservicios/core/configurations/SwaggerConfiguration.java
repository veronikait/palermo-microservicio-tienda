package edu.palermo.microservicios.core.configurations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
       info = @Info(
               title = "Tienda Palermo API",
               description = "Tienda Palermo Microservicio",
               contact = @Contact(
                       name = "Rafael Benedettelli",
                       url = "http://palermo.edu.ar",
                       email = "rbened@palermo.edu.ar"
               ),
               license = @License(
                       name = "MIT Licence",
                       url = "https://github.com/thombergs/code-examples/blob/master/LICENSE")),
       servers = @Server(url = "http://localhost:8080/palermo/microservicios/tienda/api/v1/")
)
public class SwaggerConfiguration {

}
