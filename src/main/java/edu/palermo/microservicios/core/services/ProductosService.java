package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.model.Producto;

import java.util.List;

public interface ProductosService {

    public List<Producto> getAllProductos() throws InterruptedException;

}
