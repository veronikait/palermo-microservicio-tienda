package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.model.Producto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("LOCAL") //Estereotipo Spring.
@Slf4j
@ConditionalOnProperty(prefix = "productos",name = "ubicacion",havingValue = "NEGOCIO")
public class ProductosServiceImpl implements ProductosService {

    public ProductosServiceImpl(){
        log.info("Creando una instancia de ProductosServiceImpl");
    }

    //Repositorio fake de productos.
    List<Producto> productos = new ArrayList<>(Arrays.asList(
            new Producto(1,"PC HP","i3"),
            new Producto(2,"PC LENOVO","i5"),
            new Producto(3,"PC IBM","i7")
    ));

    public List<Producto> getAllProductos(){
        //Logica de negocios compleja
        //... Mucho codigo.

        return productos;
    }




}
