package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.model.Marca;

import java.util.List;

public interface MarcaService {

    public List<Marca> getAllMarcas();

    public Marca createMarca(Marca marca);
}
