package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.clients.ProductosClientImpl;
import edu.palermo.microservicios.core.configurations.MailMessageProperties;
import edu.palermo.microservicios.core.model.Producto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("LOCAL") //Estereotipo Spring.
@Slf4j
@RequiredArgsConstructor
@ConditionalOnProperty(prefix = "productos",name = "ubicacion",havingValue = "VINTAGE")
public class ProductosServiceVintageImpl implements ProductosService {

    @NonNull
    ProductosClientImpl productosClient;

    @NonNull
    JavaMailSenderImpl mailSender;

    @NonNull
    SimpleMailMessage mailMessageAlternativo;

    @NonNull
    MailMessageProperties mailMessageProperties;

    public List<Producto> getAllProductos()  {

        //Mensaje del mail
        log.info("Enviando mail...... {} ", mailMessageAlternativo.getSubject());
        log.info("Envio mail");
        mailMessageAlternativo.setText("nuevo mensaje modificado");
        log.info("Mensaje del mail {} ", mailMessageProperties);
        //mailSender.send(simpleMailMessage);

        //recupera desde otra api
        List<Producto> productos = productosClient.retrieveProducts();
        return productos;
    }




}
