package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.dao.entities.MarcaEntity;
import edu.palermo.microservicios.core.dao.repositories.MarcaRepository;
import edu.palermo.microservicios.core.mappers.MarcaMapper;
import edu.palermo.microservicios.core.model.Marca;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MarcaServiceImpl implements MarcaService {

    @Autowired
    MarcaRepository marcaRepository;

    @Autowired
    MarcaMapper marcaMapper;

    @Override
    public List<Marca> getAllMarcas() {

        //List<Marca> marcas = marcaRepository.findByPrecioGreaterThan(200);
        List<MarcaEntity> marcasEntities = marcaRepository.findAll();
        List<Marca> marcasDtos = marcaMapper.mapping(marcasEntities);
        return marcasDtos;
    }

    @Override
    public Marca createMarca(Marca marca) {

        MarcaEntity marcaEntity = new MarcaEntity(
                marca.getId(),
                marca.getNombre(),
                marca.getDetail(),
                marca.getPrecio()
        );

        MarcaEntity marcaCreated = marcaRepository.save(marcaEntity);
        return marca;


    }
}
