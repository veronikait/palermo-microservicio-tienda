package edu.palermo.microservicios.core.services;

import edu.palermo.microservicios.core.model.Producto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@Primary //Importancia.
@Service("DEPO") //Estereotipo Spring.
@Lazy
@Slf4j
@ConditionalOnProperty(prefix = "productos",name = "ubicacion",havingValue = "DEPOSITO")
public class ProductosServiceDepoImpl implements ProductosService {

    public ProductosServiceDepoImpl(){
        log.info("Creando una instancia de ProductosServiceDepoImpl");
    }

    //Repositorio fake de productos.
    List<Producto> productos = new ArrayList<>(Arrays.asList(
            new Producto(1,"PC Samsung","i3"),
            new Producto(2,"PC DELL","i5"),
            new Producto(3,"PC MAC","i7")
    ));

    public List<Producto> getAllProductos(){
        //Logica de negocios compleja
        //... Mucho codigo.

        return productos;
    }




}
