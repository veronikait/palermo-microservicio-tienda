package edu.palermo.microservicios.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import edu.palermo.microservicios.core.validators.Cuit;
import edu.palermo.microservicios.core.validators.groups.OnCreate;
import edu.palermo.microservicios.core.validators.groups.OnUpdate;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Cliente.",name = "Cliente tienda")
@JsonPropertyOrder({"nombre","edad","id"})
public class Cliente {

    @Schema(description = "Unique identifier of the Contact.",
            example = "1", required = true)
    private Integer id;
    //@NonNull
    @NotNull(groups = OnCreate.class,message = "El nombre no puede ser nulo")
    @NotBlank(groups= OnUpdate.class,message = "No puede ser vacio")
    @JsonProperty("name")
    private String nombre;

    @NotNull(groups= OnCreate.class)
    private String password;

    @Min(value = 18, message = "Debe ser mayor de edad",groups = OnCreate.class)
    @Max(value = 80, message = "Debe ser menor de 80",groups = OnCreate.class)
    private Integer edad;

    @Cuit
    @JsonIgnore
    private String cuit;
}
