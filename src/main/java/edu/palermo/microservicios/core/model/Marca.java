package edu.palermo.microservicios.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Marca {

    private Long id;
    private String nombre;
    private String detail;

    private Integer precio;

}
