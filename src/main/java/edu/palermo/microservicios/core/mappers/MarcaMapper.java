package edu.palermo.microservicios.core.mappers;

import edu.palermo.microservicios.core.dao.entities.MarcaEntity;
import edu.palermo.microservicios.core.model.Marca;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper public interface MarcaMapper {

    @Mapping(source = "detalle",target = "detail")
    List<Marca> mapping(List<MarcaEntity> marcaEntities);

}
