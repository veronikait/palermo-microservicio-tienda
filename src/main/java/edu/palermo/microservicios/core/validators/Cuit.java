package edu.palermo.microservicios.core.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CUITValidator.class)
@Target( { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Cuit {
  String message() default "Invalido número CUIT";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
}



