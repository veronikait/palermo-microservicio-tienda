package edu.palermo.microservicios.core.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CUITValidator implements ConstraintValidator<Cuit, String>{

 @Override
 public boolean isValid(String value, ConstraintValidatorContext context) {

    if (value == null) {
       return false;
    }

    //30-29319653-3
    return value.length()==13;
 }

}
