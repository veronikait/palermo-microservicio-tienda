package edu.palermo.microservicios.core.dao.repositories;

import edu.palermo.microservicios.core.dao.entities.MarcaEntity;
import edu.palermo.microservicios.core.model.Marca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarcaRepository extends JpaRepository<MarcaEntity,Long> {

    List<MarcaEntity> findByPrecioGreaterThan(Integer precio);
}
