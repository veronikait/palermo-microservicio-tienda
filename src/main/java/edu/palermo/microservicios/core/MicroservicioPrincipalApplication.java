package edu.palermo.microservicios.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class MicroservicioPrincipalApplication {
	public static void main(String[] args) {
		SpringApplication.run(MicroservicioPrincipalApplication.class, args);
	}

}
