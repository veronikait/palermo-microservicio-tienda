package edu.palermo.microservicios.core.clients;

import edu.palermo.microservicios.core.model.Producto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class ProductosClientImpl {

    public List<Producto> retrieveProducts(){
        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<List<Producto>> response = restTemplate.
                exchange("http://192.241.182.27/prods.json",
                        HttpMethod.GET, null, new ParameterizedTypeReference<List<Producto>>() {
                        });

        List<Producto> productos = response.getBody();
        return productos;
    }

}
