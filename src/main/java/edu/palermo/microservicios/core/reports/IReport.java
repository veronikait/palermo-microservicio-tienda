package edu.palermo.microservicios.core.reports;

import edu.palermo.microservicios.core.model.Producto;

import java.util.List;

public interface IReport {

    public void report(List<Producto> productos) throws InterruptedException;
}
