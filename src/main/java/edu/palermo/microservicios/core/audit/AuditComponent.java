package edu.palermo.microservicios.core.audit;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AuditComponent {

   @Pointcut("within(@edu.palermo.microservicios.core.stereotypes.Report *)")
   public void auditReportClasses(){};

   @Around("auditReportClasses()")
   public void auditMethods(ProceedingJoinPoint jp) throws Throwable {

       long start = System.nanoTime();

       log.info("ejecuta antes");
       Object proceed = jp.proceed();
       log.info("ejecuta despues");
       long end = System.nanoTime();
       String methodName = jp.getSignature().getName();
       System.out.println("Ejecuto metodo " + methodName + " en " + (end - start));
   }
}
