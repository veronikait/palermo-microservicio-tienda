package edu.palermo.microservicios.core.controllers;

import edu.palermo.microservicios.core.model.Marca;
import edu.palermo.microservicios.core.services.MarcaService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class MarcaControllerRest {

    @NonNull
    private MarcaService marcaService;

    @GetMapping("/marcas")
    public ResponseEntity<?> getAllMarcas() {

        List<Marca> marcas = marcaService.getAllMarcas();
        return ResponseEntity.ok(marcas);
    }

    @PostMapping("/marcas")
    public ResponseEntity<?> altaMarca(
            @RequestBody Marca marca) {

        Marca marcaCreated = marcaService.createMarca(marca);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(marcaCreated.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

}
