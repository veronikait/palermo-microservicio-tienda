package edu.palermo.microservicios.core.controllers;

import edu.palermo.microservicios.core.model.Cliente;
import edu.palermo.microservicios.core.model.Producto;
import edu.palermo.microservicios.core.reports.IReport;
import edu.palermo.microservicios.core.services.ProductosService;
import edu.palermo.microservicios.core.services.ProductosServiceImpl;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/productos")
@Tag(name = "API Productos",
        description = "CRUD de productos de tienda")
public class ProductoControllerRest {

    //@Qualifier("DEPO")
    //Reduciendo un poco el acoplamiento.
    @Autowired //Indica a Spring que inyecte una instancia de la clase ProductosServiceImpl
    @Lazy
    ProductosService productosService; //<--- Inyeccion a cargo de Spring en tiempo Arranque.

    @Autowired
    IReport report;

    @GetMapping
    public ResponseEntity<?> listAll() throws InterruptedException {

        //Alto Acoplamiento
        //ProductosServiceImpl productosService = new ProductosServiceImpl();
        List<Producto> productos = productosService.getAllProductos();

        return ResponseEntity.ok(productos);
    }

    @GetMapping("/pdf")
    public ResponseEntity<?> report() throws InterruptedException {
        log.info("ejecutando enpoint pdf...." + Thread.currentThread().getName());
        List<Producto> productos = productosService.getAllProductos();
        report.report(productos);
        return ResponseEntity.noContent().build();
    }

}
