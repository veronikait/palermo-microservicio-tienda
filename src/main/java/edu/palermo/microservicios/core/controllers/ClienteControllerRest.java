package edu.palermo.microservicios.core.controllers;

import edu.palermo.microservicios.core.exceptions.BadRequestException;
import edu.palermo.microservicios.core.exceptions.ResourceNotFoundException;
import edu.palermo.microservicios.core.model.Cliente;
import edu.palermo.microservicios.core.validators.groups.OnCreate;
import edu.palermo.microservicios.core.validators.groups.OnUpdate;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/clientes")
@Slf4j
@Tag(name = "API Clientes",
        description = "CRUD de clientes de tienda")
public class ClienteControllerRest {


    List<Cliente> clientes = new ArrayList<>(Arrays.asList(
            new Cliente(1,"Rafael","pepe1234",20,"30-29319653-3"),
            new Cliente(2,"Fernando","pepe3343",21,"30-29319653-4"),
            new Cliente(3,"Pepito","pepe12232",22,"30-29319653-5")
    ));


    @Operation(summary = "Recupera un cliente por Id", description = "Recupera un cliente dado un id de tipo numerico")
    @ApiResponse(responseCode = "200", description = "Operación exitosa",content = @Content(schema = @Schema(implementation = Cliente.class)))
    @ApiResponse(responseCode = "400", description = "Error de petición")
    @ApiResponse(responseCode = "404", description = "Recurso no encontrado")
    @GetMapping("/{id}")
    public ResponseEntity<?> getClienteById(
            @Parameter(description="Id del cliente. Valor entero", required=true,example = "1")
            @PathVariable Integer id){

        log.info("Se esta ejecutando el cliente por id {}", id);

        if (id < 1){
            throw new BadRequestException("El id debe ser mayor o igual a 1");
        }

        for (Cliente cliente : clientes) {
            if (cliente.getId().equals(id)){
                return ResponseEntity.ok(cliente);
            }
        }

        throw new ResourceNotFoundException("Cliente no encontrado");
    }

    @Cacheable("clientes")
    @GetMapping
    @ApiResponse(responseCode = "200", description = "Operación exitosa",content = @Content(array = @ArraySchema(schema = @Schema(implementation = Cliente.class))))
    public ResponseEntity<?> listAllClientes(){
        log.info("Ejecutand endpoint get clientes");
        return ResponseEntity.ok(clientes);
    }

    @Operation(summary = "Alta de un cliente", description = "alta cliente")
    @PostMapping
    @CacheEvict("clientes")
    public ResponseEntity<?> altaCliente(
            @Parameter(name = "JSON de cliente",description = "JSON de cliente completo", schema=@Schema(implementation = Cliente.class))
            @Validated(OnCreate.class)
            @RequestBody Cliente cliente){

        clientes.add(cliente);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(cliente.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping
    public ResponseEntity modificarCliente(
            @RequestBody @Validated(OnUpdate.class) Cliente cliente){

        //1. Paso 1: recuperar cliente por id
        Cliente clienteEncontrado = clientes.
                stream().
                filter(cli -> cli.getId().equals(cliente.getId())).
                findFirst().orElseThrow(() -> new ResourceNotFoundException("cliente no encontrado"));

        //2. Paso 2: Modificar atributos del cliente
        clienteEncontrado.setNombre(cliente.getNombre());
        clienteEncontrado.setPassword(cliente.getPassword());

        return ResponseEntity.noContent().build();

    }

    @DeleteMapping("/{id}")
    public ResponseEntity eliminarCliente(@PathVariable Integer id){

        //1. Paso 1: recuperar cliente por id
        Cliente clienteEncontrado = clientes.
                stream().
                filter(cli -> cli.getId().equals(id)).
                findFirst().orElseThrow();

        //2. Paso 2: elimino el cliente de la lista
        clientes.remove(clienteEncontrado);

        return ResponseEntity.noContent().build();

    }

}
