package edu.palermo.microservicios.core.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaMundoRest {

    // http://localhost:8080/saludo/Rafael

    @GetMapping("/saludo/{nombre}")
    public String saludo(@PathVariable String nombre){
        return "Bienvenido " + nombre;
    }

}
